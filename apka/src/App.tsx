import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import Home from './widoki/home';
import Comment from './widoki/comment';
import User from './widoki/user';

function App() {
  return (
    <div>
      <BrowserRouter>
          <Switch>
            <Route path="/user" component={User}/>
            <Route path="/comment" component={Comment}/>
            <Route path="/" component={Home}/>
          </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;