import React,{useState,useEffect}from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import '../App.css';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
let posts = [];
let comments = [];

fetch('https://jsonplaceholder.typicode.com/comments')
    .then((res) => res.json())
    .then(output => {
        comments= output;
        console.log(comments);
    }).catch(error => console.log(error))


fetch('https://jsonplaceholder.typicode.com/posts')
    .then((res) => res.json())
    .then(output => {
        posts= output;
        console.log(posts);
    }).catch(error => console.log(error))


class Home extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            currentPostIdClicked: 0,
            isAccountHistoryDialogOpen: false,
        }
    }

    openPostDetailsDialog= (id) => this.setState({ isPostDetailsDialogOpen: true,  currentPostIdClicked: id })
    closePostDetailsDialog = () => this.setState({ isPostDetailsDialogOpen: false })

    render() {
        return (
            <div>
            <h1 id='title'>Posty</h1>
            <table>
            <thead>
            <tr>
            <th className={"row"}>Tytuł</th>
            <th className={"row"}>Tresc</th>
            </tr>
            </thead>
            <tbody>
            {posts.map((data, key) => {
                    return (
                        <tr key={key}>
                        <td className={"row"}>{data.title}</td>
                        <td className={"row"}>{data.body}</td>
                        <button className={"btn btn-info row"} onClick={() => this.openPostDetailsDialog(data.id)}>Pokza komentarze</button>
                    </tr>
                )
                })}
            </tbody>
            </table>
        {
        <Dialog open={this.state.isPostDetailsDialogOpen} onClose={this.closePostDetailsDialog} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Komentarze do posta</DialogTitle>
        <DialogContent>
        <TableContainer component={Paper}>
            <Table aria-label="simple table">
            <TableHead>
            <TableRow>
            <TableCell>Imię</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Tresc</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {comments.filter(row => row.postId == this.state.currentPostIdClicked).map((row) => (
                    <TableRow key={row.id} >
                <TableCell align="left">{row.name}</TableCell>
                <TableCell align="left">{row.email}</TableCell>
                <TableCell align="left">{row.body}</TableCell>
                </TableRow>
        ))}
        </TableBody>
        </Table>
        </TableContainer>
        </DialogContent>
        <DialogActions>
        <Button onClick={this.closePostDetailsDialog} color="primary">
            Zamknij
            </Button>
            </DialogActions>
            </Dialog>
        }
    <br></br>
        </div>
    )

    }

}
export default Home;
