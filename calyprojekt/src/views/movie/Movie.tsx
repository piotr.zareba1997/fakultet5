import React from 'react';
import NavPanel from '../../components/navPanel/NavPanel'
import { useParams } from 'react-router-dom';

import movieService, { IMovieResponse } from '../../services/movies.service';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import Button from '@material-ui/core/Button';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    btnUlubiony:{

    },
    box:{
     textAlign:'center',
     fontSize:'25px',
     color:'grey',
     padding:'2px',
    },
    box2:{
      textAlign:'center',
      fontSize:'15px',
     color:'grey'
     },
    ocena:{
      height:'60px',
      background:'yellow',
      color:'black',
      textAlign:'center',
      fontSize:'30px',
    },
    glosy:{
      textAlign:'center',
      height:'60px',
      
      background:'grey',
      color:'white',
      fontSize:'30px',
      
    },
    root: {
      flexGrow: 1,
    },
    paper: {
      background:'#F9F9F9',
      marginTop:'20px',
      padding: theme.spacing(5),
      margin: 'auto',
      maxWidth: 700,
      border:'1px solid blue'

    },
    image: {

    },
    img: {
      margin: 'auto',
      display: 'block',
      maxWidth: '100%',
      maxHeight: '100%',
    },
  }),
);


const Movie = () => {
    const {id} = useParams<{id:string}>();

    const [movie, setMovie] =  React.useState<IMovieResponse | null>(null)

  React.useEffect(() => {
    movieService.searchById(id).then((resp) => {
        if(resp){
            setMovie(resp)
            console.log(resp)
        }
    });

    //movieService.searchById('tt0848228');
    //movieService.searchById('tt3896198');
  }, []);
  console.log(movie?.Title)
  console.log(`id: ${id}`);

    const classes = useStyles();
    return (
        <div >
            <NavPanel />
            <div className={classes.paper}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                  <Typography gutterBottom variant="h3" component={'span'}  >
                      {movie?.Title} {" "}{movie?.Year}
                  </Typography>
               </Grid>

                <Grid item xs={6}>
                    <ButtonBase className={classes.image}>
                        <img className={classes.img} alt="complex" src={movie?.Poster} />
                    </ButtonBase>
                </Grid>

                <Grid item xs={6} sm container>
                        <div className='btnUlubiony'>
                        <Button variant="outlined" color="secondary" component={'span'} >
                          Dodaj do ulubionych
                        </Button>   
                        </div>
                        <Grid item xs={12}>
                        <Typography  component={'span'} variant="body2" gutterBottom >
                                <Grid container spacing={2}>
                                  <Grid item xs={6} className={classes.box2}> 
                                    ocena
                                  </Grid>
                                   
                                  <Grid item xs={6} className={classes.box2}>
                                     głosy
                                  </Grid>
                                  <Grid item xs={6} className={classes.ocena}> 
                                    {movie?.Ratings[0].Value }
                                  </Grid>
                                   
                                  <Grid item xs={6} className={classes.glosy}>
                                     {movie?.imdbVotes }
                                  </Grid>
                              </Grid>
                          </Typography>
                          </Grid>
                          <Grid item xs={12}>
                        <Typography component={'span'} variant="body2" gutterBottom>
                        
                          <Typography component={'span'} variant="body2" gutterBottom>
                            Typ: <strong>{movie?.Type}</strong>
                          </Typography>
                          
                          <br/>
                          <Typography component={'span'} variant="body2" gutterBottom>
                            Kraj: <strong>{movie?.Country}</strong>
                          </Typography>
                          <br/>
                          <Typography component={'span'} variant="body2" gutterBottom>
                            Rezyser: <strong>{movie?.Director}</strong>
                          </Typography>
                          <br/>

                          <Typography component={'span'} variant="body2" gutterBottom>
                          Produkcja: <strong> {movie?.Production}</strong>
                          </Typography>
                          <br/>

                          <Typography component={'span'} variant="body2" gutterBottom>
                          Zrealizowany:  <strong>{movie?.Released }</strong>
                          </Typography>
                          <br/>
                          <Typography component={'span'} variant="body2" gutterBottom>
                          Czas trwania:  <strong>{movie?.Runtime} </strong>
                          </Typography>
                          <br/>

                        </Typography>
                        </Grid>

                    </Grid >

                    <Grid item xs={12}>
                        <Typography component={'span'} variant="body2" gutterBottom>
                          Plot: {movie?.Plot}
                        </Typography>
                        <br/>
                        <Typography component={'span'} variant="body2" gutterBottom>
                          Aktorzy:  <strong>{movie?.Actors} </strong>
                        </Typography>

                    </Grid>
                    
                </Grid>
      </div>
                

        </div>
    );
};

export default Movie;