import React, { useEffect } from 'react';
import MenuBar from '../menuBar/MenuBar';
import DrawerComponent from '../drawerComponent/DrawerComponent'


const NavPanel = () => {
    const [drawerOpen, setDrawerOpen] = React.useState(false);

    return (
        <div >
           <MenuBar onIconClick={() => setDrawerOpen(true)  }/>
            <DrawerComponent shouldBeOpen={drawerOpen} setShouldBeOpen={setDrawerOpen} />          
        </div>
    );
}

export default NavPanel;