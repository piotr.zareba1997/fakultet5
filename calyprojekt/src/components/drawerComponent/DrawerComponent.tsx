import React, { useEffect } from 'react';
import { Drawer } from '@material-ui/core';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { useHistory } from 'react-router';

import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Typography from '@material-ui/core/Typography';
import HomeIcon from '@material-ui/icons/Home';
import SearchIcon from '@material-ui/icons/Search';
import MovieIcon from '@material-ui/icons/Movie';


const makeClasses = makeStyles((theme: Theme) => ({
    drawerContent: {
        margin: '20px',
        textAlign:'left',
    },
    menuChoice: {
        padding:'10px',
        listStyle:'none',

        '&:hover': {
            color:'white',
            background:'blue',
        },
    },


}));


interface IDrawerComponentProps {
    shouldBeOpen: boolean,
    setShouldBeOpen:CallableFunction,
}

const DrawerComponent: React.FC<IDrawerComponentProps> = ({ shouldBeOpen, setShouldBeOpen}) => {
    const classes = makeClasses();
    //const [isOpen, setIsOpen] = React.useState(false);
    const history = useHistory();

    //console.log("should = ",shouldBeOpen);
    // console.log("io = ",isOpen);


    const RedirectTo = (path: string, name: string) => <div onClick={() => history.push(path)}>{name}</div>
    return (
        <div >
            <Drawer 
                open={   shouldBeOpen }
                onClose={() =>setShouldBeOpen(false)}
            >
                <div className={classes.drawerContent} >
                <MenuItem>
                        <ListItemIcon>
                             <HomeIcon fontSize="small" />
                        </ListItemIcon>
                        <Typography variant="inherit"> {RedirectTo('/', 'Strona Główna')} </Typography>
                    </MenuItem>
                    <MenuItem>
                        <ListItemIcon>
                             <MovieIcon fontSize="small" />
                        </ListItemIcon>
                        <Typography variant="inherit">{RedirectTo('/movie', 'Filmy')}</Typography>
                    </MenuItem>
                    <MenuItem>
                        <ListItemIcon>
                            <SearchIcon fontSize="small" />
                        </ListItemIcon>
                        <Typography variant="inherit" noWrap>
                             {RedirectTo('/search', 'Szukaj filmu')}
                        </Typography>
                    </MenuItem>
                </div>
            </Drawer>
        </div>
    );
};

export default DrawerComponent;