﻿namespace KoronaWirusMonitor3.Models
{
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortCut { get; set; }
    }
}
