﻿using FluentValidation;
using KoronaWirusMonitor3.Models;

namespace KWMonitor.Validators
{
    public class CityValidator : AbstractValidator<City>
    {
        CityValidator()
        {
            RuleFor(city => city.Name).NotEmpty()
                    .WithMessage("Nazwa Miasta nie może być pusta.")
                    .MinimumLength(2)
                    .WithMessage("Nazwa miasta musi być dłuższa niż 2 znaki.")
                    .MaximumLength(50);
        }
    }
}
